import React, { Component } from 'react';
import { Image, ListItem, Avatar } from 'react-native-elements';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
 
const catNames = require('cat-names');
export default class Main extends Component {
  array = [{}];
  state = {
    cats: [],
    textInput_Holder: ''
  };
 
  componentDidMount() {
    this.setState({ cats: [...this.array] })
  }
 
  setCats = (num) => {
    for(let i=0;i < num; i++){
      this.array.push({
        image : 'https://placekitten.com/200/300',
        title : catNames.random()}
      );
    }
    this.setState({ cats: [...this.array]})
  }
 
  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#607D8B",
        }}
      />
    );
  };

  renderItem = ({ item }) => (
    <ListItem 
      Avatar={<Avatar style={{width:100, height:100}}/>}
      source={{ uri:'https://placekitten.com/50/50' }}
      title={item.title}
    />
  );
 
  render() {
    return (
      
      <View style={styles.MainContainer}>
      <View>
      <Image 
          style={{width: 50, height: 50}}
          source={{uri: 'https://www.mathworks.com/responsive_image/100/0/0/0/0/cache/matlabcentral/profiles/757722_1523844738328_DEF.jpg'}}
        />
       </View>
        <View style= {{flexDirection:'row'}}>
        <TouchableOpacity onPress={() => this.setCats(30)} activeOpacity={0.7} style={styles.button} >
          <Text style={styles.buttonText}> 30 </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.setCats(50)} activeOpacity={0.7} style={styles.button} >
          <Text style={styles.buttonText}> 50 </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.setCats(100)} activeOpacity={0.7} style={styles.button} >
          <Text style={styles.buttonText}> 100 </Text>
        </TouchableOpacity>
        
        </View>
       
       <FlatList
          data={this.state.cats}
          // width='100%' // Nera tokio propso ->  https://facebook.github.io/react-native/docs/flatlist
          keyExtractor={(index) => index.toString()}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          renderItem= {this.renderItem}
        />
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
 
  MainContainer: {
 
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    margin: 2
 
  },
 
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
 
  textInputStyle: {
 
    textAlign: 'center',
    height: 40,
    width: '90%',
    borderWidth: 1,
    borderColor: '#4CAF50',
    borderRadius: 7,
    marginTop: 12
  },
 
  button: {
    width: '30%',
    height: 40,
    padding: 10,
    backgroundColor: '#4CAF50',
    borderRadius: 8,
    marginTop: 10,
    margin: 5
  },
 
  buttonText: {
    color: '#fff',
    textAlign: 'center',
  },
});