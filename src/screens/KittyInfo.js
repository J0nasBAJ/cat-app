/* eslint-disable no-unused-vars */
import React, { Component } from "react";

import { ListItem, Avatar } from 'react-native-elements'; // Unused imports
import {
  Text, // Unused import
  View,
  Image,
  Button, // Unused import
  Platform, // Unused import
  StyleSheet,
} from 'react-native';

export default class KittyInfo extends Component{
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Image style={{width: 50, height: 50}} source={{ uri: 'https://reactjs.org/logo-og.png' }}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
  },
});
