/* eslint-disable no-unused-vars */
import React from 'react';
import Main from './screens/Main'
import KittyInfo from './screens/KittyInfo'

import { createStackNavigator, createAppContainer } from 'react-navigation'
import {
  YellowBox,
  StyleSheet,   // Unused import
  Text,         // Unused import
  View,         // Unused import
  FlatList,     // Unused import
  Dimensions    // Unused import
} from 'react-native';

YellowBox.ignoreWarnings([ 'Warning: isMounted(...) is deprecated', 'Module RCTImageLoader' ]);

const AppStackNavigator = createStackNavigator({
  Main: { screen: KittyInfo },
  KittyInfo: { screen: KittyInfo },
});

const AppContainer = createAppContainer(AppStackNavigator);


const App = () => (
    <View style={{ flex: 1, backgroundColor: 'red' }}>
      <Text>Test 123</Text>
    </View>
);
export default App;
